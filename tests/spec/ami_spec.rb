#Parse the JSON file created by create-ami-json.sh from the Packer manifests. Then test the AMI IDs in the Packer manifests

require 'spec_helper'
require 'json'

json_file_path = 'ami.json'

ami_file = JSON.parse(File.read(json_file_path))

CurrentAmi = (ami_file['ami'])

describe ami(CurrentAmi) do
  it { should exist }
end

describe ami(CurrentAmi) do
  it { should be_available }
end
