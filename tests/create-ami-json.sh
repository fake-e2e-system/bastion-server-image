#extract the AMI ID from the Packer manifest and create a JSON file list.

ami_id=$(cat ../packer/manifest.json | jq -r '.builds[-1].artifact_id' |  cut -d':' -f2)

cat >./ami.json <<EOF

{
"ami":"$ami_id"
}

EOF
